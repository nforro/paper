# paper

This package enables users to indicate their preferred paper size, provides
the paper(1) utility to find the user's preferred default paper size and give
information about known sizes, and specifies system-wide and per-user paper
size catalogs, which can be can also be used directly (see paperspecs(5)).